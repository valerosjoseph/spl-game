compile:
	javac Animator.java
	javac BoundingBox.java
	javac Instance.java
	javac Step.java
	javac Test.java
	javac Window.java

clean:
		rm *.class
