public class Animator implements Runnable
{
	public Window window;

	public Animator(Window window)
	{
		this.window = window;
	}

	public void run()
	{
		while(true)
		{
			window.panel.repaint();

			for(int i = 0; i < window.instances.size(); i++)
			{
				Instance instance = window.instances.get(i);
				instance.sprite.inc();
			}

			try
			{
				Thread.sleep(1000/15);
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
	}
}
