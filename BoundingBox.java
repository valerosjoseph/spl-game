class BoundingBox
{
  public int width;
  public int height;
  BoundingBox(int width, int height)
  {
    this.width = width;
    this.height = height;
  }
}
