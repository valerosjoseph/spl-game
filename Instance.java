public class Instance
{
		public Sprite sprite;
		public int x, y;
		public BoundingBox boundingBox;
		public boolean state = false;
		public boolean solid = false;
		public int speed;
		public String direction;
		public Instance()
		{
			direction = "right";
			speed = 0;
			sprite = new Sprite();
			boundingBox = new BoundingBox(64,64);
		}
		public void set_sprite(Sprite sprite)
		{
			this.sprite = sprite;
			this.sprite.set_instance(this);
		}
		public void addX(int i)
		{
			x += i;
		}

	  public boolean collidesWith(Instance bb)
	  {

			int aax = x,
				aay = y,
				aawidth = boundingBox.width,
				aaheight = boundingBox.height;
			int bbx = bb.x,
				bby = bb.y,
				bbwidth = bb.boundingBox.width,
				bbheight = bb.boundingBox.height;
				// System.out.println(aax + " " + aay + " " + aawidth + " " + aaheight);
				// System.out.println(bbx + " " + bby + " " + bbwidth + " " + bbheight);
				// System.out.println();
		 	if (aax <= bbx + bbwidth &&
					aax + aawidth >= bbx &&
				  aay <= bby + bbheight &&
					aay + aaheight >= bby)
			{
				return true;
			}
			else
				return false;
	  }
}
