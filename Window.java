import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.image.*;


import java.util.ArrayList;
import java.util.List;

public class Window extends JFrame implements KeyListener
{
	public BufferedImage img;
	public JPanel panel;
	public int instanceCount;
	public String gameState;
	// public Sprite player;
	// public Sprite[] character;

	//
	public Instance inventory[];
	public Sprite player_fly;
	public Sprite player_still_right;
	public Sprite player_still_left;
	public Sprite player_walk_right;
	public Sprite player_walk_left;
	public Sprite player_attack_right;
	public Sprite player_attack_left;
	public Sprite spr_dummy;
	public Sprite spr_dummy2;
	public Sprite spr_trap1;
	public Instance player;
	public Instance dummy1;
	public Instance dummy2;
	public Trap trap1;
	public List <Instance> instances;


	public void init_sprites()
	{
		player_walk_right = new Sprite();
		player_walk_left = new Sprite();
		player_still_right = new Sprite();
		player_still_left = new Sprite();
		player_attack_right = new Sprite();
		player_attack_left = new Sprite();
		spr_dummy = new Sprite();
		spr_dummy2 = new Sprite();
		spr_trap1 = new Sprite();

		player_walk_right.name = "sprites/walk_right";
		player_walk_left.name = "sprites/walk_left";
		player_still_right.name = "sprites/still_right";
		player_still_left.name = "sprites/still_left";
		player_attack_right.name = "sprites/attack_right";
		player_attack_left.name = "sprites/attack_left";

		player_walk_right.addImage("sprites/right1.png");
		player_walk_right.addImage("sprites/right2.png");
		player_walk_right.addImage("sprites/right3.png");
		player_walk_left.addImage("sprites/left1.png");
		player_walk_left.addImage("sprites/left2.png");
		player_walk_left.addImage("sprites/left3.png");

		player_still_right.addImage("sprites/right1.png");
		player_still_left.addImage("sprites/left1.png");

		player_attack_right.addImage("sprites/attack_right.png");
		player_attack_left.addImage("sprites/attack_left.png");

		spr_dummy.addImage("sprites/left1.png");
		spr_dummy2.addImage("sprites/right1.png");
		spr_trap1.addImage("sprites/right1.png");

	}
	public void init_instances()
	{
		instances = new ArrayList<Instance>();
		player = new Instance();
		dummy1 = new Instance();
		dummy2 = new Instance();
		trap1 = new Trap();


		player.set_sprite(player_still_right);
		dummy1.set_sprite(spr_dummy);
		dummy2.set_sprite(spr_dummy2);
		dummy1.x = 400;
		dummy2.x = 200;
		dummy1.y = 1;
		trap1.set_sprite(spr_trap1);
		trap1.x = 600;
		// dummy2.y = 100;
		player.speed = 5;
		instances.add(player);
		instances.add(dummy1);
	  instances.add(trap1);

	}
	public void initGame()
	{
		inventory = new Instance[3];
		gameState = "Initializing";
	}
	public void initialize()
	{
		instanceCount = 0;
		init_sprites();
		init_instances();
		initGame();
	}

	public void update()
	{
		Step step = new Step(this);
		Thread tStep = new Thread(step);
		tStep.start();
	}
	public void draw()
	{
		Animator a = new Animator(this);
		Thread ani = new Thread(a);
		ani.start();
	}
	public Window()
	{
		initialize();
		panel = new JPanel()
		{
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				for(int i = 0; i < instances.size(); i++)
			 	{
					instances.get(i).sprite.paint(g);
			 	}
			}
		};

		add(panel);
		addKeyListener(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1024, 768);
		setVisible(true);

		draw();
		update();
	}
	public void addItemToInventory(Instance i)
	{
		inventory[0] = i;
	}

	public void moveR()
	{
		instances.get(0).x = instances.get(0).x + instances.get(0).speed;
		player.set_sprite(player_walk_right);
		player.direction = "right";
	}

	public void moveL()
	{
		instances.get(0).x = instances.get(0).x - instances.get(0).speed;
		player.set_sprite(player_walk_left);
		player.direction = "left";
	}

	public void moveU()
	{
		instances.get(0).y = instances.get(0).y - instances.get(0).speed;
		// player.set_sprite(player_);
	}

	public void moveD()
	{

		instances.get(0).y = instances.get(0).y + instances.get(0).speed;
		// player.set_sprite(player_walk);
	}


	public void action1()
	{
		if(inventory[0] != null)
			inventory[0].x = player.x;
		// else
		// 	player.set_sprite(player_attack_left);

	}

	public void keyTyped(KeyEvent e)
	{
  }

  public void keyPressed(KeyEvent e)
  {
  	if(e.getKeyChar() == 'a')
			moveL();
		else if(e.getKeyChar() == 'd')
			moveR();
		else if(e.getKeyChar() == 'w')
			moveU();
		else if(e.getKeyChar() == 's')
			moveD();
		else if(e.getKeyChar() == '1')
			action1();
		else
		{
			if(player.direction == "right")
				player.set_sprite(player_still_right);
			else
				player.set_sprite(player_still_left);
		}
		// System.out.println(e.getKeyChar());
  }
  public void keyReleased(KeyEvent e)
  {
		if(player.direction == "right")
			player.set_sprite(player_still_right);
		else
			player.set_sprite(player_still_left);
  }
}
