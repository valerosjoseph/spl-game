import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.image.*;

import java.util.ArrayList;
import java.util.List;
public class Sprite
{

	public List <BufferedImage> images;
	public JPanel panel;
	public int x, y;
	public boolean state = false;
	public int ctr;
	public Instance instance;
	public String name;
	public Sprite()
	{
		name = new String("");
		ctr = x = y = 0;
		images = new ArrayList<BufferedImage>();
	}

	public void addImage(String path)
	{
		try
		{
			images.add(ImageIO.read(new File(path)));
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	public void paint(Graphics g)
	{
		g.drawImage(images.get(ctr), instance.x, instance.y, null);
	}
	public void set_instance(Instance instance)
	{
		this.instance = instance;
	}
	public void inc()
	{
		ctr = (ctr+1) % images.size();
	}

}
